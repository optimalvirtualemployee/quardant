$(document).ready(function() {

        $('.nav-menu a, #mobile-nav a, .scrollto').on('click', function() {
            if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {
                var target = $(this.hash);
                if (target.length) {
                    var top_space = 0;

                    if ($('#header').length) {
                        top_space = $('#header').outerHeight();

                        if (!$('#header').hasClass('header-fixed')) {
                            top_space = top_space;
                        }
                    }

                    $('html, body').animate({
                        scrollTop: target.offset().top - top_space
                    }, 1500, 'easeInOutExpo');

                    if ($(this).parents('.nav-menu').length) {
                        $('.nav-menu .menu-active').removeClass('menu-active');
                        $(this).closest('li').addClass('menu-active');
                    }

                    if ($('body').hasClass('mobile-nav-active')) {
                        $('body').removeClass('mobile-nav-active');
                        $('#mobile-nav-toggle i').toggleClass('lnr-times lnr-bars');
                        $('#mobile-body-overly').fadeOut();
                    }
                    return false;
                }
            }
        });

     


    /* -------------------------------------------------------------------
        Header Scroll Class js
     ------------------------------------------------------------------ */

        $(window).scroll(function() {
            if ($(this).scrollTop() > 100) {
                $('.site-header').addClass('sticky');
            } else {
                $('.site-header').removeClass('sticky');
            }
        });


    /* -------------------------------------------------------------------
        Owl Carusel js
     ------------------------------------------------------------------ */


     $('.client-carousel').owlCarousel({
        loop:true,
        margin:0,
        nav:false,
        dots:false,
        autoplay:true,
        autoplaySpeed:1000,

        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
    });
    
    $('.project-carousel').owlCarousel({
        stagePadding: 50,
        loop:true,
        margin:10,
        nav:false,
        dots:false,
        responsive:{
            0:{
                items:1,
                stagePadding: 20
            },
            575:{
                items:2
            }
        }
    })
    
    $('.inner-project-carousel').owlCarousel({
        stagePadding: 50,
        loop:true,
        margin:10,
        nav:false,
        dots:false,
        responsive:{
            0:{
                items:1,
                stagePadding: 20
            },
            575:{
                items:2
            }
        }
    })
    
    $('.articals-carousel').owlCarousel({
        stagePadding: 50,
        loop:true,
        margin:10,
        nav:false,
        dots:false,
        responsive:{
            0:{
                items:1,
                stagePadding: 20
            },
            575:{
                items:2
            }
        }
    })
    
    $('.services-carousel').owlCarousel({
        stagePadding: 50,
        loop:true,
        margin:10,
        nav:false,
        dots:false,
        responsive:{
            0:{
                items:1,
                stagePadding: 20
            },
            575:{
                items:2
            }
        }
    })
    
    $('.navbar-menu').click(function(){
        $('body').toggleClass('open-menu');
        
        if ($(this).hasClass('change')){
            $(this).removeClass('change');
        } else {
            $(this).addClass('change');
        };
        $('.mobile_body').hide();
    });
    
    $('.has-sub-menu span').on('click',function(event){
        event.preventDefault();
        $('[data-menu='+$(this).parent().attr("data-open")+']').fadeIn(350);
    })
    
    $(".select > span").click(function() {
        $(this).parents('.mobile_body').fadeOut();
    });
});

    $('.navbar-nav li').click(function(){
        $('.navbar-nav li').removeClass('active');
        $(this).addClass('active');
    });
    
    
    
    
    
